﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

namespace Snake_and_ladder
{
    internal class Player:Dice
    {
        protected int turn;
        
        public Player()
        {
            turn = 0;
        }

        public int Switch()
        {
            if (number != 6)
            {
                if (turn == 0)
                    turn = 1;
                else
                    turn = 0;
            }
            return turn;
        }

        public int getTurn()
        {
            return turn;
        }

    }
}
