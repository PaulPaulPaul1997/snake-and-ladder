﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake_and_ladder
{
    internal class Board:Player
    {
        int A0;
        int A1;

        public void set()
        {
            A0 = 1;
            A1 = 1;
        }

        public void Calc()
        {
            if (turn == 0 && (A0+number)<=100)
            {
                A0 = A0 + number;
                if (A0 == 9)
                    A0 = 30;
                if (A0 == 19)
                    A0 = 40;
                if (A0 == 34)
                    A0 = 53;
                if (A0 == 38)
                    A0 = 64;
                if (A0 == 60)
                    A0 = 79;
                if (A0 == 67)
                    A0 = 95;
                if (A0 == 69)
                    A0 = 90;
                if (A0 == 78)
                    A0 = 97;
                if (A0 == 25)
                    A0 = 2;
                if (A0 == 29)
                    A0 = 8;
                if (A0 == 51)
                    A0 = 48;
                if (A0 == 59)
                    A0 = 18;
                if (A0 == 76)
                    A0 = 66;
                if (A0 == 92)
                    A0 = 73;
                if (A0 == 99)
                    A0 = 13;
            }
            if (turn == 1 && (A1 + number) <= 100)
            {
                A1 = A1 + number;
                if (A1 == 9)
                    A1 = 30;
                if (A1 == 19)
                    A1 = 40;
                if (A1 == 34)
                    A1 = 53;
                if (A1 == 38)
                    A1 = 64;
                if (A1 == 60)
                    A1 = 79;
                if (A1 == 67)
                    A1 = 95;
                if (A1 == 69)
                    A1 = 90;
                if (A1 == 78)
                    A1 = 97;
                if (A1 == 25)
                    A1 = 2;
                if (A1 == 29)
                    A1 = 8;
                if (A1 == 51)
                    A1 = 48;
                if (A1 == 59)
                    A1 = 18;
                if (A1 == 76)
                    A1 = 66;
                if (A1 == 92)
                    A1 = 73;
                if (A1 == 99)
                    A1 = 13;
            }
        }

        public int MoveA()
        {

            return A0;
        }

        public int MoveB()
        {             
             return A1;
        }

    }
}
