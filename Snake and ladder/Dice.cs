﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake_and_ladder
{
    internal class Dice
    {
        static Random r = new Random();
        protected static int number;

        public int Roll()
        {
            number = r.Next(1, 7);
            return number;
        }
    }
}
